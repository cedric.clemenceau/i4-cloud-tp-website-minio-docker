FROM php:5-apache

RUN apt update && apt upgrade -y && docker-php-ext-install mysqli && apt install -y git && apt install -y vim && apt-get install -y libcurl4-openssl-dev pkg-config libssl-dev
RUN service apache2 start
RUN php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
RUN php composer-setup.php
RUN php -r "unlink('composer-setup.php');"
RUN php composer.phar require aws/aws-sdk-php
RUN pecl install mongodb
RUN echo "extension=mongodb.so" >> /usr/local/etc/php/conf.d/docker-php-ext-mysqli.ini
RUN php composer.phar require mongodb/mongodb
RUN pecl install redis
RUN echo "extension=redis.so" >> /usr/local/etc/php/conf.d/docker-php-ext-mysqli.ini
RUN echo "session.save_handler = redis" >> /usr/local/etc/php/conf.d/docker-php-ext-mysqli.ini
RUN echo 'session.save_path = "tcp://redis:6379"' >> /usr/local/etc/php/conf.d/docker-php-ext-mysqli.ini
COPY www /var/www/html/


EXPOSE 80


